import logging as log
from sys import stdout
from typing import List

from flask_openapi3 import Info
from flask_openapi3 import OpenAPI
from pydantic import BaseModel, Field

from home_lights.bulb_gateway import BulbGateway
from home_lights.bulb_store import BulbStore
from home_lights.statics import Statics

log.basicConfig(stream=stdout, level=log.INFO)

bulb_store = BulbStore()
bulb_store.set_bulbs()

version = '0.2.0'
info = Info(title="House Lights Controller", description="API service to control Yeelight lights on home network.", version=version)
app = OpenAPI(__name__, info=info)


class BulbName(BaseModel):
    bulb_name: str = Field(description="Name of bulb", example="office_orb")


class BulbSet(BaseModel):
    bulbs: List[str] = Field(description="List of bulb names", example=["bedroom_tall", "office_orb"])


class PowerBody(BaseModel):
    state: str = Field(description="Power state", example='on')


class BrightnessBody(BaseModel):
    level: int = Field(description="Brightness level as percentage", example=55, ge=Statics.BRIGHTNESS_MIN, le=Statics.BRIGHTNESS_MAX)


class TemperatureBody(BaseModel):
    degrees: int = Field(description="Temperature in degrees", example=4500, ge=Statics.TEMP_MIN, le=Statics.TEMP_MAX)


class RGBBody(BaseModel):
    red: int = Field(description="Red value", example=50, ge=Statics.RGB_MIN, le=Statics.RGB_MAX)
    green: int = Field(description="Green value", example=100, ge=Statics.RGB_MIN, le=Statics.RGB_MAX)
    blue: int = Field(description="Blue value", example=200, ge=Statics.RGB_MIN, le=Statics.RGB_MAX)


class ColourBody(BaseModel):
    rgb: RGBBody = Field(description="RGB colour values")


class BulbChangeResponse(BaseModel):
    message: str = Field(description="Response message", example="ok")


@app.get("/bulbs", summary="Get all configured bulbs", responses={"200": BulbSet})
def get_bulbs():
    return {'bulbs': list(bulb_store.get_bulbs().keys())}


@app.post("/bulbs/reset", summary="Reset all bulbs on network",  responses={"200": BulbChangeResponse})
def reset_bulbs():
    return bulb_store.set_bulbs()


@app.post("/bulb/<string:bulb_name>/power", summary="Set power state of bulb", responses={"200": BulbChangeResponse})
def toggle(path: BulbName, body: PowerBody):
    if body.state == 'toggle':
        return BulbGateway.toggle(bulb_store.get_bulb(path.bulb_name))
    elif body.state == 'on':
        return BulbGateway.on(bulb_store.get_bulb(path.bulb_name))
    elif body.state == 'off':
        return BulbGateway.off(bulb_store.get_bulb(path.bulb_name))
    else:
        raise Exception(f"Could not determine requested power state: '{body.state}'")


@app.post("/bulb/<bulb_name>/brightness", summary="Set brightness of bulb", responses={"200": BulbChangeResponse})
def brightness(path: BulbName, body: BrightnessBody):
    return BulbGateway.set_brightness(bulb_store.get_bulb(path.bulb_name), body.level)


@app.post("/bulb/<bulb_name>/temperature", summary="Set temperature of bulb", responses={"200": BulbChangeResponse})
def temperature(path: BulbName, body: TemperatureBody):
    return BulbGateway.set_temperature(bulb_store.get_bulb(path.bulb_name), body.degrees)


@app.post("/bulb/<bulb_name>/colour", summary="Set colour of bulb", responses={"200": BulbChangeResponse})
def colour(path: BulbName, body: ColourBody):
    return BulbGateway.set_colour(bulb_store.get_bulb(path.bulb_name), body.rgb.red, body.rgb.green, body.rgb.blue)


if __name__ == '__main__':
    app.run(debug=True)
