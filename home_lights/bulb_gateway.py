import logging
from yeelight import Bulb, discover_bulbs

from home_lights.statics import Statics


class BulbGateway:

    @staticmethod
    def get_bulbs():
        found_bulbs = discover_bulbs()
        bulb_summary = []
        for bulb in found_bulbs:
            bulb_summary.append({
                'ip': bulb['ip'],
                'name': bulb['capabilities']['name'],
                'power': bulb['capabilities']['power']})
        return bulb_summary

    @staticmethod
    def on(bulb: Bulb):
        logging.info(f"Turning bulb '{bulb}' ON")
        return bulb.turn_on()

    @staticmethod
    def off(bulb: Bulb):
        logging.info(f"Turning bulb '{bulb}' OFF")
        return bulb.turn_off()

    @staticmethod
    def toggle(bulb: Bulb):
        logging.info(f"Toggling bulb '{bulb}'")
        return bulb.toggle()

    @staticmethod
    def set_brightness(bulb: Bulb, brightness: int):
        return bulb.set_brightness(brightness)

    @staticmethod
    def set_brightness_min(bulb: Bulb):
        return bulb.set_brightness(Statics.BRIGHTNESS_MIN)

    @staticmethod
    def set_brightness_max(bulb: Bulb):
        return bulb.set_brightness(Statics.BRIGHTNESS_MAX)

    @staticmethod
    def set_colour(bulb: Bulb, red: int, green: int, blue: int):
        return bulb.set_rgb(red, green, blue)

    @staticmethod
    def set_temperature(bulb: Bulb, temp: int):
        # min-max = 1700-6500
        logging.info(f"Set bulb {bulb} temperature to {temp}")
        return bulb.set_color_temp(temp)

    @staticmethod
    def set_temperature_min(bulb: Bulb):
        logging.info(f"Set bulb {bulb} temperature to min")
        bulb.set_color_temp(Statics.TEMP_MIN)

    @staticmethod
    def set_temperature_max(bulb: Bulb):
        logging.info(f"Set bulb {bulb} temperature to max")
        bulb.set_color_temp(Statics.TEMP_MAX)


