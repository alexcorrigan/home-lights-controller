import logging as log

from yeelight import Bulb

from home_lights.bulb_gateway import BulbGateway


class BulbStore:

    def __init__(self):
        self.configured_bulbs = {}
        self.set_bulbs()

    def set_bulbs(self):
        self.configured_bulbs.clear()
        discovered_bulbs = BulbGateway.get_bulbs()
        for discovered_bulb in discovered_bulbs:
            bulb_ip = discovered_bulb['ip']
            bulb = Bulb(bulb_ip)
            self.configured_bulbs[discovered_bulb['name']] = bulb
            log.info(f"Configured bulb: {discovered_bulb['name']} at {bulb_ip}")
        return "ok"

    def get_bulb(self, bulb_name: str):
        return self.configured_bulbs[bulb_name]

    def get_bulbs(self):
        return self.configured_bulbs
