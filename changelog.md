# Changelog

All notable changes to this project will be documented in this file.

## [0.2.0] - 2023-01-21

### Added

- Interactive OpenAPI docs (Swagger, ReDoc, RapiDoc).

## [0.1.0] - 2023-01-21

### Added

- Basic API to control discovered bulbs on network.