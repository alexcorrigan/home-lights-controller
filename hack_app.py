from yeelight import Bulb

from home_lights.bulb_gateway import BulbGateway
from home_lights.colours import Colours

import logging as log
import sys
from time import sleep

from home_lights.statics import Statics

log.basicConfig(stream=sys.stdout, level=log.INFO)

BULB_CONFIG = {
    'office_orb': {'ip': '192.168.0.40'},
    'office_strip': {'ip': '192.168.0.42'},
    'bedroom_tall': {'ip': '192.168.0.41'}
}

bulbs = {}

for bulb_name in BULB_CONFIG.keys():
    bulb_ip = BULB_CONFIG[bulb_name]['ip']
    bulb = Bulb(bulb_ip)
    bulbs[bulb_name] = bulb
    log.info(f"Configured bulb: {bulb.get_properties()['name']} at {bulb_ip}")

bulb_gateway = BulbGateway

if __name__ == "__main__":
    # bulb_gateway.toggle(bulbs['office_strip'])
    # bulb_gateway.toggle(bulbs['office_orb'])
    # bulb_gateway.set_brightness_min(bulbs['office_strip'])
    # sleep(4)
    # bulb_gateway.set_brightness_max(bulbs['office_strip'])

    office_strip = bulbs['office_strip']
    office_orb = bulbs['office_orb']
    bedroom_tall = bulbs['bedroom_tall']

    office_strip.set_name("office_light_strip")
    office_orb.set_name("office_orb")
    bedroom_tall.set_name("bedroom_tall")

    # bulb_gateway.on(office_strip)
    # bulb_gateway.on(office_orb)
    # bulb_gateway.set_temperature_min(office_orb)
    # bulb_gateway.set_brightness(office_orb, 35)
    #
    # bulb_gateway.set_colour(office_strip, *Colours.BLUE)

    # bulb_gateway.on(office_strip)
    # bulb_gateway.set_colour(office_strip, *Colour.RED)
    # sleep(1)
    # bulb_gateway.set_colour(office_strip, *Colour.GREEN)
    # sleep(1)
    # bulb_gateway.set_colour(office_strip, *Colour.BLUE)
    # sleep(1)
    # bulb_gateway.set_brightness_max(office_strip)
    # bulb_gateway.set_temperature(office_strip, 1700)
    # bulb_gateway.on(office_orb)
    # bulb_gateway.set_brightness_max(office_orb)

