# Home Lights Controller

API service to control lights on my home network. Specifically for the Yeelight brand.

Makes heavy use of the python [Yeelight](https://pypi.org/project/yeelight/) library.

## Running

### Locally
```shell
source venv/bin/activate
pip install -r requirements.txt
python3 -m flask run --host=0.0.0.0
```
Interactive OpenAPI docs can then be found at [http://localhost:5000/openapi/](http://localhost:5000/openapi/)


### Docker
[todo]