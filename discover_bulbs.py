from yeelight import discover_bulbs

bulbs = discover_bulbs()

for bulb in bulbs:
    # print(bulb)
    print(f"{bulb['ip']} -- {bulb['capabilities']['name']} -- {bulb['capabilities']['power']}")

